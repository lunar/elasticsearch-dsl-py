Source: elasticsearch-dsl-py
Maintainer: Jérémy Bobbio <jeremy.bobbio@irq7.fr>
Section: python
Priority: optional
Build-Depends: debhelper (>= 9),
               dh-python,
               python3-all,
               python3-coverage,
               python3-dateutil,
               python3-elasticsearch,
               python3-mock,
               python3-pytest,
               python3-pytest-cov,
               python3-pytest-mock,
               python3-setuptools,
               python3-six,
               python3-tz,
Standards-Version: 4.3.0
Homepage: https://github.com/elasticsearch/elasticsearch-dsl-py
Vcs-Git: https://salsa.debian.org/lunar/elasticsearch-dsl-py.git
Vcs-Browser: https://salsa.debian.org/lunar/elasticsearch-dsl-py/

Package: python3-elasticsearch-dsl
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Description: Python client for Elasticsearch (high-level library)
 Elasticsearch DSL is a high-level library whose aim is to help with writing and
 running queries against Elasticsearch. It is built on top of the official
 low-level client.
 .
 It provides a more convenient and idiomatic way to write and manipulate
 queries. It stays close to the Elasticsearch JSON DSL, mirroring its
 terminology and structure. It exposes the whole range of the DSL from Python
 either directly using defined classes or a queryset-like expressions.
 .
 It also provides an optional wrapper for working with documents as Python
 objects: defining mappings, retrieving and saving documents, wrapping the
 document data in user-defined classes.
 .
 To use the other Elasticsearch APIs (eg. cluster health) just use the
 underlying client.
